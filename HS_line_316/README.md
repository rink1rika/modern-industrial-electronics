## Описание 

В 316 кабинете стоит линия. Я сам её собирал из нескольких станций, и очень много с ней провёл времени. Сейчас есть идея сделать классный демонстрационный стенд, чтобы пестрить красивыми словами перед заказчиками. Суть стенда в распределённом управлении линией. 

Есть 4 станции Festo, каждый элемент которой присоединён к мосту, ведущему к входам и выходам ПЛК через толстый кабель с проводами ( со стороны ПЛК). 
В моей задумке предполагается, что ПЛК будет использован в качестве модулей ввода-вывода. Сами же ПЛК будут запаролены для загрузки и выгрузки проекта, чтобы никто не мог туда попасть, кроме узкого круга лиц. Работа ПЛК в "режиме" модулей ввода-вывода означает, что их физические вводы (inputs) и выводы (outputs) будут собраны на одном из ПЛК (Handling station) в формате OPC UA сервера. Таким образом, управлять этими ПЛК можно удалённо через OPC UA протокол, который соответствует шине OPAS.
Сама система управления (логика, программа) будет крутиться на сервере в виде нескольких контейнеров или нескольких виртуальных машин. Логика будет выполнена через рантайм mplc
Управлять линией можно будет как через пульт, так и через веб скада систему (MASTERSCADA4D)

Подробности можно на схеме посмотреть

## Техническое задание

Результатом работы по данному проекту является рабочая линия в соответствии со следущей архитектурой:
![](HS_line_316.drawio.png)

Технологический процесс линии:
1. По кнопке старт выбрасывается шайба из магазина на станции HS
2. Шайба захватывается гриппером и переносится на станцию ProcS (самая левая)
3. На станции ProcS шайба проходит полный цикл - определяется её цвет, определяется её положение (перевёрнутая или нет)
4. Если шайба дном вниз, то выполняется имитация сверления - опускается сверло, сверлит  и поднимается, далее шайба переносится в исходное положение на этой станции
5. Гриппер забирает шайбу, передвигается с ней к станции упаковки и зависает над ней
6. Станция упаковки разворачивает коробку, раскрывает её
7. Гриппер опускается с шайбой, бросает её в коробку, поднимается
8. Коробка закрывается
9. Гриппер забирает коробку и переносится с ней на станцию сортировки
10. Коробка на станции сортировки движется на тот скат (наклонная поверхность), который соответствует её цвету. Пусть для серебряных заготовок будет первый встречный скат, для красных второй, для чёрных последний.

Более подробно смотреть на картинке. Жду вопросы по заданию и архитектуре. Если появится вдохновение, опишу подробнее

## Ролевая модель команды разработки системы ввода-вывода ПЛК SIEMENS

| Функция                                       | Количество | Навыки                                                                           |
|-----------------------------------------------|------------|----------------------------------------------------------------------------------|
| Разработчик системы ввода-вывода  ПЛК SIEMENS | 2          | языки МЭК 61131-3, Tia Portal, TCP/IP промышленные протоколы, Agile Kanban, Jira |
| Механик                                       | 1          | Отвёртка, шестигранники, сила воли, стрессоустойчивость, православие             |
| Лид разработки                                | 1          | языки МЭК 61131-3, Tia Portal, TCP/IP промышленные протоколы, Agile Kanban, Jira |


## Ролевая модель команды разработки SCADA-системы
| Функция                   | Количество | Навыки                                                                                                                     |
|---------------------------|------------|----------------------------------------------------------------------------------------------------------------------------|
| Разработчик SCADA-системы | 3          | Знание IEC 61131-3, опыт работы со SCADA-системами, MasterSCADA, TCP/IP модель, промышленные протоколы, Agile Kanban, Jira |
| Лид разработки            | 1          | Знание IEC 61131-3, опыт работы со SCADA-системами, MasterSCADA, TCP/IP модель, промышленные протоколы, Agile Kanban, Jira |


## Ролевая модель команды разработки СУ на языках МЭК 61131-3
| Функция        | Количество | Навыки                                                                                                |
|----------------|------------|-------------------------------------------------------------------------------------------------------|
| Разработчик СУ | 3          | Знание IEC 61131-3, MasterSCADA, masterplc, TCP/IP модель, промышленные протоколы, Agile Kanban, Jira |
| Лид разработки | 1          | Знание IEC 61131-3, MasterSCADA, masterplc, TCP/IP модель, промышленные протоколы, Agile Kanban, Jira |

